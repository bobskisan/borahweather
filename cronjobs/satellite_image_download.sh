 #!/bin/bash 	

cd /var/www/html/weather/satimages

# getting date 
d=`TZ=UTC+1 date '+%Y%m%d%H'`
d="$d"30.jpg

filename=IDE00135.$d
# get image
url=http://www.bom.gov.au/gms/$filename
echo $url
wget $url

mv satimage2.jpg satimage1.jpg
mv satimage3.jpg satimage2.jpg
mv satimage4.jpg satimage3.jpg 
mv satimage5.jpg satimage4.jpg
mv satimage6.jpg satimage5.jpg
mv $filename satimage6.jpg

rm out.gif
ffmpeg -framerate 2   -pattern_type glob   -i '*.jpg'   -r 15   -vf scale=600:-1   out.gif




old(){
# Rename satimage files to previous one
		cd /var/www/html/weather/satimages
		
	mv satimage2.jpg satimage1.jpg
		mv satimage3.jpg satimage2.jpg
		mv satimage4.jpg satimage3.jpg


	# Create latest sat image url
		d= date -u '+%Y%m%d%H'30.jpg
	echo %d 	
	# morning URL before 10 am on 12th	http://www.bom.gov.au/gms/IDE00135.201902112330.jpg  9:30 am AEST  23:30 UTC on 11th		
		# morning url http://www.bom.gov.au/gms/IDE00135.201902120030.jpg    at 10:30 am  AEST  00:30 UTC

		# Afternoon url http://www.bom.gov.au/gms/IDE00135.201902120230.jpg  at 12:30 pm  AEST  02:30 UTC  201902120230.jpg
		# get latest sat image file and rename to latest satimage 

		#wget http://www.bom.gov.au/gms/IDE00135.`%d`
		#echo " http://www.bom.gov.au/gms/IDE00135.`%d` "
	
		mv IDE00135.201902100030.jpg satimage4.jpg

	# Delete out.GIF file and Create latest animated GIF

		rm -f out.gif

		sudo  ffmpeg   -framerate 2   -pattern_type glob   -i '*.jpg'   -r 15   -vf scale=512:-1   out.gif
}
