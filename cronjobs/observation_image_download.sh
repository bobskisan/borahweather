#!/bin/bash
# You're wondering what this script is meant to do, right?
# It gets the radar image from the BoM site and dumps it as a .png into /var/www/html/weather/radimages/obs/.
# It does this by]
#	1. downloading the HTML of the entire page that contains the image
#	2. finding the name of the actual .png that gets displayed on that page

cd /var/www/html/weather/radimages/obs/

# assign to the content variable the command that'll get us the BoM page's HTML. wget is just a Linux command line thing.
# wget -q keeps the output quiet
# wget -O specifies which file wget should output to
content=$(wget www.bom.gov.au/products/IDR692.loop.shtml#skip -q -O -)

# append to our command the actual name of the file we want to dump the BoM page's HTML into.
cat <<< "$content" > "Page.txt"

# extract the line from the Page.txt file which has the words obsImg and png in it
# I don't think this does anything because the sed lines following do it all again and the output of this line does not seem to get saved anywhere.
awk '/obsImg/,/png/' Page.txt

# sed -n put sed in quiet mode
# extracts FROM Page.txt any lines containing EITHER obsImg OR png and dumps them into output.html 
sed -n "/obsImg/,/png/p" Page.txt > output.html

# extract from output.html any lines containing EITHER radar OR png and dump it into tsi.html
sed -n "/radar/,/png/p" output.html > tsi.html

# cut out from (what should be just one line in) tsi.html the path of the .png that I(Bob) and presumably interested in.
ts=$(cut -d '"' -f2 < tsi.html)

# delete all .png files in the working (current) directory presumably in anticipation of download this particular one
rm *.png

# get the only the .png file with this specially constructed URL which, mercifully, the BoM responds to
wget "http://www.bom.gov.au$ts"

# rename _all_ .png files (and there should only be one at this stage) to obs1.png
mv *.png obs1.png

#Remove all other interemdiate files
rm *.html
rm *.txt

#composite -blend 99 obs6.png /var/www/html/weather/radimages/rad.gif obs1.png

# draw some text on the obs1.png that denotes where Borah is on that map
convert -pointsize 12 -fill black -draw 'text 300,230 "*Mt Borah" ' obs1.png obs.png
